using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Playground
{
    class Program
    {
        static int numValid = 0;
        static void Main(string[] args)
        {
            ParseDocuments("input.txt");

            Console.WriteLine($"Valid documents: {numValid}");
            Console.ReadLine();
        }

        private static bool IsDocumentValid(string document)
        {
            Regex regex;
            string byr = @"byr:[1][9][2-9][0-9]( |$)|byr:[2][0][0][0-2]( |$)";
            string iyr = @"iyr:[2][0][1][0-9]( |$)|iyr:[2][0][2][0]( |$)";
            string eyr = @"eyr:[2][0][2][0-9]( |$)|eyr:[2][0][3][0]( |$)";
            string hgt = @"hgt:[5][9]in( |$)|hgt:[6][0-9]in( |$)|hgt:[7][0-6]in( |$)|hgt:[1][5-8][0-9]cm( |$)|hgt:[1][9][0-3]cm( |$)";
            string hcl = @"hcl:#[0-9a-f]{6}( |$)";
            string ecl = @"(ecl:(amb|blu|brn|gry|grn|hzl|oth))( |$)";
            string pid = @"pid:[0-9]{9}( |$)";

            regex = new Regex(byr);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(iyr);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(eyr);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(hgt);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(hcl);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(ecl);
            if (!regex.IsMatch(document))
                return false;

            regex = new Regex(pid);
            if (!regex.IsMatch(document))
                return false;

            return true;
        }

        private static void ParseDocuments(string filePath)
        {
            string line;
            StringBuilder document = new StringBuilder();

            StreamReader file = new StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line) || string.IsNullOrWhiteSpace(line))
                {
                    if (IsDocumentValid(document.ToString()))
                    {
                        numValid++;
                    }
                    document.Clear();
                    continue;
                }

                document.Append(line + " ");
            }
        }

    }
}
