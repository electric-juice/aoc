using System;
using System.IO;
using System.Text;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input = File.ReadAllLines("input.txt");
            int sum = SumAnswers(input);

            Console.WriteLine(sum);
            Console.ReadLine();
        }

        private static int SumAnswers(string[] input)
        {
            StringBuilder group = new StringBuilder();
            int people = 0, sum = 0;

            foreach (string line in input)
            {
                if(string.IsNullOrEmpty(line))
                {
                    string g = group.ToString();
                    for (int i = 97; i < 123; i++)
                    {
                        if ((g.Split((char)i).Length - 1) != people)
                            continue;

                        sum++;
                    }
                    people = 0;
                    group.Clear();
                }
                else
                {
                    people++;
                    group.Append(line);
                }
            }
            return sum;
        }
    }
}
