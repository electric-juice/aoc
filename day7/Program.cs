using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Playground
{
    class Program
    {
        static List<Bag> bags;
        static void Main(string[] args)
        {
            
            var lines = File.ReadAllLines("input.txt");
            bags = new List<Bag>();

            foreach (var line in lines)
            {
                ParseaBag(line.Replace("bags", "").Replace("bag", ""));
            }

            int a = CountBags(bags, "shiny gold");

            Console.WriteLine(a-1);
            Console.ReadLine();
        }

        private static int CountBags(List<Bag> bags, string bagName)
        {
            int c = 1;
            var currentBag = bags.SingleOrDefault(x => x.Name == bagName);
            foreach (var innerBag in currentBag.Bags)
            {
                c += innerBag.Count * CountBags(bags, innerBag.BagName);
            }

            return c;
        }

        private static int ParseaBag(string line)
        {
            string patteren = @"(.*)\scontain\s(.*)";
            var match = Regex.Match(line, patteren);
            var _bag = match.Groups[1].Value.Trim();
            var _containingBags = match.Groups[2].Value.Trim().Split(',');

            Bag bag = new Bag() { Name = _bag, Visited = false };

            foreach (var containBag in _containingBags)
            {
                var temp = containBag.Trim(' ', '.');
                if(temp.Equals("no other"))
                {
                    continue;
                }

                BagSet bagset = new BagSet() { BagName = temp.Substring(2) , Count = int.Parse(temp.Substring(0, 1)) };
                bag.Bags.Add(bagset);
            }

            bags.Add(bag);
            return 0;
        }
         
        //private static void Contains(List<Bag> bags, string bagName)
        //{
        //    foreach (var bag in bags)
        //    {
        //        if (!bag.Visited && bag.Bags.Any(x => x.Contains(bagName)))
        //        {
        //            sum++;
        //            bag.Visited = true;
        //            Contains(bags, bag.Name);
        //        }
        //    }
        //}
    }

    public class Bag
    {
        public string Name { get; set; }
        public List<BagSet> Bags = new List<BagSet>();
        public bool Visited { get; set; }
    }

    public class BagSet
    {
        public string BagName { get; set; }
        public int Count { get; set; }
    }
}
