using System;
using System.Collections.Generic;
using System.IO;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input = File.ReadAllLines("input.txt");
            List<int> seatIds = new List<int>();

            int max = int.MinValue;
            foreach (var line in input)
            {
                int seatID = ParseAndConvert(line);
                seatIds.Add(seatID);
                if(seatID > max)
                    max = seatID;
            }

            seatIds.Sort();
            for (int i = 0; i < seatIds.Count-1; i++)
            {
                if((seatIds[i+1] - seatIds[i]) > 1)
                {
                    Console.WriteLine($"{seatIds[i + 1]} - {seatIds[i]} = {seatIds[i + 1] - seatIds[i]}");
                }
            }

            Console.WriteLine($"\n{max}");
            Console.ReadLine();
        }

        private static int ParseAndConvert(string input)
        {
            string row = input.Substring(0, 7).Replace("F", "0").Replace("B", "1");
            int rowNum = Convert.ToInt32(row, 2);
            string column = input.Substring(7, 3).Replace("L", "0").Replace("R", "1");
            int columnNum = Convert.ToInt32(column, 2);

            return (rowNum * 8) + columnNum;
        }

    }
}
